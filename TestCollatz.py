#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(23, 23)
        self.assertEqual(v, 16)

    def test_eval_6(self):
        v = collatz_eval(201, 999799)
        self.assertEqual(v, 525)

    def test_eval_7(self):
        v = collatz_eval(782, 999218)
        self.assertEqual(v, 525)

    def test_eval_8(self):
        v = collatz_eval(219766, 114644)
        self.assertEqual(v, 386)

    def test_eval_9(self):
        v = collatz_eval(153182, 489641)
        self.assertEqual(v, 449)

    def test_eval_10(self):
        v = collatz_eval(76650, 936638)
        self.assertEqual(v, 525)

    def test_eval_11(self):
        v = collatz_eval(522280, 500911)
        self.assertEqual(v, 470)

    def test_eval_12(self):
        v = collatz_eval(84389, 196445)
        self.assertEqual(v, 383)

    def test_eval_13(self):
        v = collatz_eval(704124, 789169)
        self.assertEqual(v, 504)

    def test_eval_14(self):
        v = collatz_eval(1, 3333)
        self.assertEqual(v, 217)

    def test_eval_15(self):
        v = collatz_eval(236573, 239905)
        self.assertEqual(v, 350)

    def test_eval_16(self):
        v = collatz_eval(436493, 439825)
        self.assertEqual(v, 400)

    def test_eval_17(self):
        v = collatz_eval(520963, 610052)
        self.assertEqual(v, 452)

    def test_eval_18(self):
        v = collatz_eval(317951, 391087)
        self.assertEqual(v, 441)

    def test_eval_19(self):
        v = collatz_eval(1000, 1)
        self.assertEqual(v, 179)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
